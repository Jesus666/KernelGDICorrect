#pragma once
#include "NtHread.h"
namespace Thread
{
	NTSTATUS	ApcpQuerySystemProcessInformation(PSYSTEM_PROCESS_INFO * SystemInfo);
	PVOID       GetWin32ThreadAddress(PEPROCESS pEProcess);

#ifdef __cplusplus
	extern "C"
	{
#endif
	PVOID NTAPI PsGetThreadWin32Thread(IN PETHREAD Thread);

	PVOID NTAPI PsGetCurrentThreadWin32Thread(VOID);

	PVOID NTAPI PsSetThreadWin32Thread(_Inout_ PETHREAD Thread,_In_ PVOID Win32Thread,_In_ PVOID OldWin32Thread);


#ifdef __cplusplus
	}
#endif
}