#pragma once
#include "Process.h"
#include "GDIMacro.h"
typedef unsigned short GLYPH;

typedef struct
{
	BOOL	state;
	int		countdown;
	BOOL	started;
	int		runlen;
	int		blippos;
	int		bliplen;
	int		length;
	GLYPH	*glyph;

} MATRIX_COLUMN;
typedef struct
{
	WORD	message[MAXMSG_WIDTH][MAXMSG_HEIGHT];
	int		msgindex;
	int		counter;
	WORD	random_reg1;
	int		width, height;
} MATRIX_MESSAGE;

typedef struct
{
	int				width;
	int				height;
	int				numcols;
	int				numrows;
	HDC				hdcBitmap;
	HBITMAP			hbmBitmap;
	MATRIX_MESSAGE *message;
	MATRIX_COLUMN	column[1];
} MATRIX;

typedef struct _RGN_ATTR
{
	ULONG AttrFlags;
	ULONG iComplexity;     /* Clipping region's complexity. NULL, SIMPLE & COMPLEXREGION */
	RECTL Rect;
} RGN_ATTR, *PRGN_ATTR;

typedef struct _DC_ATTR
{
	PVOID pvLDC;
	ULONG ulDirty_;
	HANDLE hbrush;
	HANDLE hpen;
	COLORREF crBackgroundClr;
	ULONG ulBackgroundClr;
	COLORREF crForegroundClr;
	ULONG ulForegroundClr;
	COLORREF crBrushClr;
	ULONG ulBrushClr;
	COLORREF crPenClr;
	ULONG ulPenClr;
	DWORD iCS_CP;
	INT iGraphicsMode;
	BYTE jROP2;
	BYTE jBkMode;
	BYTE jFillMode;
	BYTE jStretchBltMode;
	POINTL ptlCurrent;
	POINTL ptfxCurrent;
	LONG lBkMode;
	LONG lFillMode;
	LONG lStretchBltMode;
	FLONG flFontMapper;
	LONG lIcmMode;
	HANDLE hcmXform;
	HCOLORSPACE hColorSpace;
	FLONG flIcmFlags;
	INT IcmBrushColor;
	INT IcmPenColor;
	PVOID pvLIcm;
	FLONG flTextAlign;
	LONG lTextAlign;
	LONG lTextExtra;
	LONG lRelAbs;
	LONG lBreakExtra;
	LONG cBreak;
	HANDLE hlfntNew;
	MATRIX mxWorldToDevice;
	MATRIX mxDeviceToWorld;
	MATRIX mxWorldToPage;
	FLOATOBJ efM11PtoD;
	FLOATOBJ efM22PtoD;
	FLOATOBJ efDxPtoD;
	FLOATOBJ efDyPtoD;
	INT iMapMode;
	DWORD dwLayout;
	LONG lWindowOrgx;
	POINTL ptlWindowOrg;
	SIZEL szlWindowExt;
	POINTL ptlViewportOrg;
	SIZEL szlViewportExt;
	FLONG flXform;
	SIZEL szlVirtualDevicePixel;
	SIZEL szlVirtualDeviceMm;
	SIZEL szlVirtualDeviceSize;
	POINTL ptlBrushOrigin;
	RGN_ATTR VisRectRegion;
} DC_ATTR, *PDC_ATTR;




typedef enum _DCFUNC
{
	//DCFUNC_AbortDoc,
	DCFUNC_AbortPath,
	DCFUNC_AlphaBlend, // UNIMPLEMENTED
	DCFUNC_AngleArc, // UNIMPLEMENTED
	DCFUNC_Arc,
	DCFUNC_ArcTo, // UNIMPLEMENTED
	DCFUNC_BeginPath,
	//DCFUNC_BitBlt,
	DCFUNC_Chord,
	DCFUNC_CloseFigure,
	DCFUNC_Ellipse,
	DCFUNC_EndPath,
	DCFUNC_ExcludeClipRect,
	DCFUNC_ExtEscape,
	DCFUNC_ExtFloodFill,
	DCFUNC_ExtSelectClipRgn,
	DCFUNC_ExtTextOut,
	DCFUNC_FillPath,
	DCFUNC_FillRgn,
	DCFUNC_FlattenPath,
	DCFUNC_FrameRgn,
	DCFUNC_GetDeviceCaps,
	DCFUNC_GdiComment,
	DCFUNC_GradientFill, // UNIMPLEMENTED
	DCFUNC_IntersectClipRect,
	DCFUNC_InvertRgn,
	DCFUNC_LineTo,
	DCFUNC_MaskBlt, // UNIMPLEMENTED
	DCFUNC_ModifyWorldTransform,
	DCFUNC_MoveTo,
	DCFUNC_OffsetClipRgn,
	DCFUNC_OffsetViewportOrgEx,
	DCFUNC_OffsetWindowOrgEx,
	DCFUNC_PathToRegion, // UNIMPLEMENTED
	DCFUNC_PatBlt,
	DCFUNC_Pie,
	DCFUNC_PlgBlt, // UNIMPLEMENTED
	DCFUNC_PolyBezier,
	DCFUNC_PolyBezierTo,
	DCFUNC_PolyDraw,
	DCFUNC_Polygon,
	DCFUNC_Polyline,
	DCFUNC_PolylineTo,
	DCFUNC_PolyPolygon,
	DCFUNC_PolyPolyline,
	DCFUNC_RealizePalette,
	DCFUNC_Rectangle,
	DCFUNC_RestoreDC,
	DCFUNC_RoundRect,
	DCFUNC_SaveDC,
	DCFUNC_ScaleViewportExtEx,
	DCFUNC_ScaleWindowExtEx,
	DCFUNC_SelectBrush,
	DCFUNC_SelectClipPath,
	DCFUNC_SelectFont,
	DCFUNC_SelectPalette,
	DCFUNC_SelectPen,
	DCFUNC_SetDCBrushColor,
	DCFUNC_SetDCPenColor,
	DCFUNC_SetDIBitsToDevice,
	DCFUNC_SetBkColor,
	DCFUNC_SetBkMode,
	DCFUNC_SetLayout,
	//DCFUNC_SetMapMode,
	DCFUNC_SetPixel,
	DCFUNC_SetPolyFillMode,
	DCFUNC_SetROP2,
	DCFUNC_SetStretchBltMode,
	DCFUNC_SetTextAlign,
	DCFUNC_SetTextCharacterExtra,
	DCFUNC_SetTextColor,
	DCFUNC_SetTextJustification,
	DCFUNC_SetViewportExtEx,
	DCFUNC_SetViewportOrgEx,
	DCFUNC_SetWindowExtEx,
	DCFUNC_SetWindowOrgEx,
	DCFUNC_SetWorldTransform,
	DCFUNC_StretchBlt,
	DCFUNC_StrokeAndFillPath,
	DCFUNC_StrokePath,
	DCFUNC_TransparentBlt, // UNIMPLEMENTED
	DCFUNC_WidenPath,

} DCFUNC;

typedef enum GDILoObjType
{
	GDILoObjType_LO_BRUSH_TYPE = 0x100000,
	GDILoObjType_LO_DC_TYPE = 0x10000,
	GDILoObjType_LO_BITMAP_TYPE = 0x50000,
	GDILoObjType_LO_PALETTE_TYPE = 0x80000,
	GDILoObjType_LO_FONT_TYPE = 0xa0000,
	GDILoObjType_LO_REGION_TYPE = 0x40000,
	GDILoObjType_LO_ICMLCS_TYPE = 0x90000,
	GDILoObjType_LO_CLIENTOBJ_TYPE = 0x60000,
	GDILoObjType_LO_ALTDC_TYPE = 0x210000,
	GDILoObjType_LO_PEN_TYPE = 0x300000,
	GDILoObjType_LO_EXTPEN_TYPE = 0x500000,
	GDILoObjType_LO_DIBSECTION_TYPE = 0x250000,
	GDILoObjType_LO_METAFILE16_TYPE = 0x260000,
	GDILoObjType_LO_METAFILE_TYPE = 0x460000,
	GDILoObjType_LO_METADC16_TYPE = 0x660000
} GDILOOBJTYPE, *PGDILOOBJTYPE;

typedef struct gdi_physdev
{
	const struct gdi_dc_funcs *funcs;
	struct gdi_physdev        *next;
	HDC                        hdc;
} *PHYSDEV;


typedef struct
{
	LPVOID pKernelAddress;
	USHORT wProcessId;
	USHORT wCount;
	USHORT wUpper;
	USHORT wType;
	LPVOID pUserAddress;
} GDICELL;

typedef struct _GDI_TABLE_ENTRY
{
	PVOID KernelData; /* Points to the kernel mode structure */
	DWORD ProcessId;  /* process id that created the object, 0 for stock objects */
	union {            /* temp union structure. */
		LONG  Type;       /* the first 16 bit is the object type including the stock obj flag, the last 16 bits is just the object type */
		struct {
			USHORT FullUnique; /* unique */
			UCHAR  ObjectType; /* objt */
			UCHAR  Flags;      /* Flags */
		};
	};
	PVOID UserData;   /* pUser Points to the user mode structure, usually NULL though */
} GDI_TABLE_ENTRY, *PGDI_TABLE_ENTRY;

typedef enum _POLYFUNCTYPE
{
	GdiPolyPolygon = 1,
	GdiPolyPolyLine,
	GdiPolyBezier,
	GdiPolyLineTo,
	GdiPolyBezierTo,
	GdiPolyPolyRgn,
} POLYFUNCTYPE, *PPOLYFUNCTYPE;