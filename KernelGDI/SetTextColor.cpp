#include "SetTextColor.h"


BOOL GdiGetHandleUserData(HGDIOBJ hGdiObj, DWORD ObjectType, PVOID * UserData)
{
	PEPROCESS Eprocess = IoGetCurrentProcess();
	PVOID ppeb = Process::GetProcessPeb(Eprocess);
	if (ppeb == NULL) {
		DPRINT("SetTextColor.cpp GetProcessPeb() An Error.GdiGetHandleUserData() Internal Function\r\n");
	}
	GDICELL* Entry = (GDICELL*)*(LPVOID*)((ULONG64)ppeb + 0xF8);
	if (Entry == NULL) {
		DPRINT("SetTextColor.cpp Entry == NULL An Error.GdiGetHandleUserData() Internal Function\r\n");
		return FALSE;
	}
	Entry = Entry + GDI_HANDLE_GET_INDEX(hGdiObj);
	if (Entry == NULL) {
		DPRINT("SetTextColor.cpp  Entry + GDI_HANDLE_GET_INDEX(hGdiObj) == NULL An Error.GdiGetHandleUserData() Internal Function\r\n");
		return FALSE;
	}

	if (MmIsAddressValid(Entry->pUserAddress) != TRUE) {
		DPRINT("SetTextColor.cpp  MmIsAddressValid(Entry->pUserAddress) != TRUE An Error.GdiGetHandleUserData() Internal Function\r\n");
		return FALSE;
	}

	*UserData = Entry->pUserAddress;
	return TRUE;
}

PDC_ATTR GdiGetDcAttr(HDC hdc)
{

	GDILOOBJTYPE eDcObjType;
	PDC_ATTR pdcattr;

	/* Check DC object type */
	eDcObjType = (GDILOOBJTYPE)GDI_HANDLE_GET_TYPE(hdc);
	if ((eDcObjType != GDILoObjType_LO_DC_TYPE) &&
		(eDcObjType != GDILoObjType_LO_ALTDC_TYPE))
	{
		return NULL;
	}
	if (!GdiGetHandleUserData((HGDIOBJ)hdc, eDcObjType, (PVOID*)&pdcattr))
	{
		return NULL;
	}
	return pdcattr;

}
