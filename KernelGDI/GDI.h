#pragma once
#include "ShadowSSDT.h"
#include "Process.h"
#include "Thread.h"
#include "KernelGDIFunction.h"
#include "ShadowSSDT.h"
#include "SetTextColor.h"
/*
   这样初始化
   GDIStartsCalling()
   GDIInitialization()
   GDIStopsCalling()
   调用函数一个流程
*/
class GDI
{
public:
	BOOLEAN GDIStartsCalling(HANDLE Pid, PKAPC_STATE Kpc,PULONG64  OldWin32);
	VOID    GDIStopsCalling(KAPC_STATE Kpc, ULONG64 OldThreadWin32);
public:
	NTSTATUS GDIInitialization();
private:
	NTSTATUS KernelGDIFunctionInitialization();
public:
	PVOID AllocateVirtualMemory(SIZE_T Size);
	VOID FreeVirtualMemory(PVOID VirtualAddress, SIZE_T Size);

public:
	HDC GetDc(IN HWND hwnd);
	COLORREF SetTextColor(_In_ HDC hdc, _In_ COLORREF crColor);

public:
	HWND FindWindowEx(_In_ HWND hWndParent, _In_ HWND hWndChildAfter, _In_ LPCWSTR lpszClass, _In_ LPCWSTR lpszWindow);
	HWND FindWindow(_In_   WCHAR * lpClassName, _In_ WCHAR * lpWindowName);
public:
	BOOL ExtTextOutW(_In_ HDC hdc, _In_ INT x, _In_ INT y, _In_ UINT fuOptions, _In_opt_  RECT *lprc, _In_reads_opt_(cwc) LPWSTR lpString, _In_ UINT cwc, _In_reads_opt_(cwc)  INT *lpDx);
	BOOL ExtTextOutA(_In_ HDC hdc, _In_ INT x, _In_ INT y, _In_ UINT fuOptions, _In_opt_  RECT *lprc, _In_reads_opt_(cch) LPCSTR lpString, _In_ UINT cch, _In_reads_opt_(cch)  INT *lpDx);

public:
	ULONG_PTR Polyline(_In_ HDC hdc,_In_reads_(cpt) const POINT *apt,_In_ INT cpt);
	COLORREF SetPixel(_In_ HDC hdc, _In_ INT x, _In_ INT y, _In_ COLORREF crColor);

public:

private:
	ShadowSSDT m_ShadowSSDT;
	KernelGDIFunction m_KernelGDIFunction;
};

