#include "Process.h"

NTSTATUS Process:: KeAttchProcess_Pfn(IN HANDLE ProcessId, OUT PKAPC_STATE Kpc)
{
	PEPROCESS Eprocess = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	if (ProcessId == (HANDLE)4) {
		//驱动的上下文属于System System的PID是4
		DPRINT("Process.cpp ProcessId == (HANDLE)4 An Error.KeAttchProcess_Pfn Internal Function");
		return Status;
	}

	Status = PsLookupProcessByProcessId(ProcessId, &Eprocess);
	if (!NT_SUCCESS(Status) || Eprocess == NULL) {
		//获取进程Eprocess 请检查PID是否存在
		DPRINT("Process.cpp PsLookupProcessByProcessId An Error.KeAttchProcess_Pfn Internal Function");
		return Status;
	}
	ObDereferenceObject(Eprocess);
	Status = STATUS_SUCCESS;


	KeStackAttachProcess(Eprocess, Kpc);
	return Status;
}

VOID Process::UnKeAttchProcess_Pfn(IN KAPC_STATE Kpc)
{
	KeUnstackDetachProcess(&Kpc);
}

PVOID Process::GetProcessPeb(PEPROCESS Eprocess)
{
	PVOID pPeb = PsGetProcessPeb(Eprocess);
	if (pPeb == NULL)
	{
		pPeb = PsGetProcessWow64Process(Eprocess);
		if (pPeb == NULL)
		{
			DPRINT("Process.cpp PsGetProcessWow64Process() An Error.GetProcessPeb Internal Function");
		}
	}
	return pPeb;
}
