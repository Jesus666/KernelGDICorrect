#pragma once
#include "NtHread.h"
#define MAXMSG_WIDTH	0x100
#define MAXMSG_HEIGHT	0x100
#define GDI_HANDLE_COUNT 0x10000
#define GDI_GLOBAL_PROCESS (0x0)

/* Handle Masks and shifts */
#define GDI_HANDLE_INDEX_MASK (GDI_HANDLE_COUNT - 1)
#define GDI_HANDLE_TYPE_MASK  0x007f0000
#define GDI_HANDLE_BASETYPE_MASK 0x001f0000
#define GDI_HANDLE_STOCK_MASK 0x00800000
#define GDI_HANDLE_REUSE_MASK 0xff000000
#define GDI_HANDLE_REUSECNT_SHIFT 24
#define GDI_HANDLE_UPPER_MASK 0x0000ffff

#define GM_COMPATIBLE 1
#define GM_ADVANCED 2
#define GM_LAST     2
#define MM_ANISOTROPIC 8
#define MM_HIENGLISH 5
#define MM_HIMETRIC 3
#define MM_ISOTROPIC 7
#define MM_LOENGLISH 4
#define MM_LOMETRIC 2
#define MM_TEXT 1
#define MM_TWIPS 6
#define MM_MAX_FIXEDSCALE	MM_TWIPS
#define MM_MIN MM_TEXT
#define MM_MAX MM_ANISOTROPIC
#define ABSOLUTE	1
#define RELATIVE	2
#define PC_EXPLICIT 2
#define PC_NOCOLLAPSE 4
#define PC_RESERVED 1
#define CLR_INVALID     0xFFFFFFFF
#define PT_MOVETO 6
#define PT_LINETO 2
#define PT_BEZIERTO 4
#define PT_CLOSEFIGURE 1
#define TT_AVAILABLE 1
#define TT_ENABLED 2
#define GDI_HANDLE_INDEX_MASK (GDI_HANDLE_COUNT - 1)
#define GDI_HANDLE_TYPE_MASK  0x007f0000
#define GDI_HANDLE_BASETYPE_MASK 0x001f0000
#define GDI_HANDLE_EXTYPE_MASK 0x00600000
#define GDI_HANDLE_STOCK_MASK 0x00800000
#define GDI_HANDLE_REUSE_MASK 0xff000000
#define GDI_HANDLE_REUSECNT_SHIFT 24
#define GDI_HANDLE_BASETYPE_SHIFT 16

#define GDI_ENTRY_STOCK_MASK 0x00000080
#define GDI_ENTRY_BASETYPE_MASK 0x001f0000
#define GDI_ENTRY_UPPER_SHIFT 16
#define DIRTY_FILL                          0x00000001
#define DIRTY_LINE                          0x00000002
#define DIRTY_TEXT                          0x00000004
#define DIRTY_BACKGROUND                    0x00000008
#define DIRTY_CHARSET                       0x00000010
#define SLOW_WIDTHS                         0x00000020
#define DC_CACHED_TM_VALID                  0x00000040
#define DISPLAY_DC                          0x00000080
#define DIRTY_PTLCURRENT                    0x00000100
#define DIRTY_PTFXCURRENT                   0x00000200
#define DIRTY_STYLESTATE                    0x00000400
#define DC_PLAYMETAFILE                     0x00000800
#define DC_BRUSH_DIRTY                      0x00001000
#define DC_PEN_DIRTY                        0x00002000
#define DC_DIBSECTION                       0x00004000
#define DC_LAST_CLIPRGN_VALID               0x00008000
#define DC_PRIMARY_DISPLAY                  0x00010000
#define DC_ICM_NOT_CALIBRATED               0x00020000
#define DC_ICM_BRUSH_DIRTY                  0x00040000
#define DC_ICM_PEN_DIRTY                    0x00080000
#define DC_ICM_NOT_SET                      0x00100000
#define DC_MODE_DIRTY                       0x00200000
#define DC_FONTTEXT_DIRTY                   0x00400000

#define RGB(r,g,b)          ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)))
#if defined(_X86_) && !defined(USERMODE_DRIVER)
typedef struct _FLOATOBJ
{
	ULONG  ul1;
	ULONG  ul2;
} FLOATOBJ, *PFLOATOBJ;
#else
typedef FLOAT FLOATOBJ, *PFLOATOBJ;
#endif

#define GDI_HANDLE_GET_TYPE(h)     \
    (((ULONG_PTR)(h)) & GDI_HANDLE_TYPE_MASK)

#define GDI_HANDLE_GET_INDEX(h)    \
    (((ULONG_PTR)(h)) & GDI_HANDLE_INDEX_MASK)