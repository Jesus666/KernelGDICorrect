#pragma once
#include "NtHread.h"

namespace Process
{
	NTSTATUS KeAttchProcess_Pfn(IN HANDLE ProcessId,OUT PKAPC_STATE Kpc);

	VOID UnKeAttchProcess_Pfn(IN KAPC_STATE Kpc);

	PVOID GetProcessPeb(IN PEPROCESS Eprocess);

#ifdef __cplusplus
	extern "C"
	{
#endif
	NTKERNELAPI 	PVOID     NTAPI		 PsGetProcessWin32Process(PEPROCESS Process);

	NTKERNELAPI NTSTATUS  NTAPI		 PsSetProcessWin32Process(_Inout_ PEPROCESS Process,_In_opt_ PVOID Win32Process,_In_opt_ PVOID OldWin32Process);

	NTKERNELAPI PVOID     NTAPI		 PsGetCurrentProcessWin32Process(VOID);

	NTKERNELAPI PPEB NTAPI PsGetProcessPeb(_In_ PEPROCESS Process);


	NTKERNELAPI PVOID NTAPI PsGetProcessWow64Process(
		_In_ PEPROCESS Process
	);

#ifdef __cplusplus
	}
#endif
}