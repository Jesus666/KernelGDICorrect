#pragma once
#include "NtHread.h"

class KernelGDIFunction
{
public:
	HDC(APIENTRY *NtUserGetDC)(HWND hWnd);


	BOOL(APIENTRY *NtGdiExtTextOutW)(IN HDC hDC,
		IN INT XStart,
		IN INT YStart,
		IN UINT fuOptions,
		IN OPTIONAL LPRECT UnsafeRect,
		IN LPWSTR UnsafeString,
		IN INT Count,
		IN OPTIONAL LPINT UnsafeDx,
		IN DWORD dwCodePage);

	HWND
	(APIENTRY* NtUserFindWindowEx)(HWND hwndParent,
		HWND hwndChildAfter,
		PUNICODE_STRING ucClassName,
		PUNICODE_STRING ucWindowName,
		DWORD dwUnknown);

	ULONG_PTR
	(APIENTRY* NtGdiPolyPolyDraw)(
		_In_ HDC hdc,
		_In_ PPOINT ppt,
		_In_reads_(ccpt) PULONG pcpt,
		_In_ ULONG ccpt,
		_In_ INT iFunc);

	COLORREF
	(APIENTRY* NtGdiSetPixel)(
		_In_ HDC hdc,
		_In_ INT x,
		_In_ INT y,
		_In_ COLORREF crColor);


};